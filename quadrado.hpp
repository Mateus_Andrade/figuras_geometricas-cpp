#ifndef QUADRADO_H
#define QUADRADO_H

#include "geometrica.hpp"

class quadrado : public geometrica{
    public:
        quadrado();
        quadrado(float base, float altura);

        float area();
        float area (float base, float altura);
};

#endif

