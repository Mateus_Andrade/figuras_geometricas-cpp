#include "triangulo.hpp"

triangulo::triangulo(){
    setBase(10);
    setAltura(10);
}

triangulo::triangulo(float base, float altura){
    setBase(base);
    setAltura(altura);
}

float triangulo::area(){
    return getBase() * getAltura() * 1.0/2.0;
}

float triangulo::area(float base, float altura){
        return base * altura * 1.0/2.0;
}

