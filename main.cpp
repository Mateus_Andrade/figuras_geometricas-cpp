#include "retangulo.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"

#include <iostream>

using namespace std;

int main(){

	geometrica * formaRetangulo = new retangulo(20,20);
	geometrica * formaQuadrado = new quadrado (14,15);
	geometrica * formaTriangulo = new triangulo (18,20);
	cout << "Area do retangulo: " << formaRetangulo->area() << endl;
	cout << "Area do quadrado: " << formaQuadrado->area() << endl;
	cout << "Area do triangulo: " << formaTriangulo->area() << endl; 

    return 0;
}
